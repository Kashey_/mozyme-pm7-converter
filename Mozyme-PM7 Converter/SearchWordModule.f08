module SearchWordModule
use ConstantModule
implicit none

contains

! Переписывать текст до тех пор, пока не найдём искомое слово.
! Слово тоже переписываем.
logical function searchWordRewriting(word)
    character(len=:), allocatable, intent(in)   :: word
    character(len=1)                            :: ch
    character(len=1)                            :: tch
    integer                                     :: io
    integer                                     :: q
    read(word, '(A)',iostat = io)ch
    do
        read(unitOldFile,'(A)',advance='no',iostat=io,end=400)tch
        call writeChrAndNewLine(tch, io)
        if(tch.ne.ch)cycle
        do q = 2, len(word)
            read(word(q:q+1), '(A)',end=400)ch
            read(unitOldFile,'(A)',advance='no',iostat=io,end=400)tch
            call writeChrAndNewLine(tch, io)
            if(tch.ne.ch)then
                read(word, '(A)',iostat = io)ch
                exit
            endif
            if(q.eq.len(word))then
                searchWordRewriting = .true.
                return
            endif
        enddo
    enddo
400 continue
    searchWordRewriting = .false.
end function searchWordRewriting

! Найти нужное слово, не переписывая.
logical function searchWord(word)
    character(len=:), allocatable, intent(in)   :: word
    character(len=1)                            :: ch
    character(len=1)                            :: tch
    integer                                     :: io
    integer                                     :: q
    read(word, '(A)',iostat = io)ch
    do
        read(unitOldFile,'(A)',advance='no',iostat=io,end=500)tch
        if(tch.ne.ch)cycle
        do q = 2, len(word)
            read(word(q:q+1), '(A)',end=500)ch
            read(unitOldFile,'(A)',advance='no',iostat=io,end=500)tch
            if(tch.ne.ch)then
                read(word, '(A)',iostat = io)ch
                exit
            endif
            if(q.eq.len(word))then
                searchWord = .true.
                return
            endif
        enddo
    enddo
500 continue
    searchWord = .false.
end function searchWord

! Найти нужное слово, не переписывая, но считая, строчки.
logical function searchWordCounting(word, lines)
    character(len=:), allocatable, intent(in)   :: word
    character(len=1)                            :: ch
    character(len=1)                            :: tch
    integer                                     :: io
    integer                                     :: q
    integer, intent(out)                        :: lines
    read(word, '(A)',iostat = io)ch
    lines = 0
    do
        read(unitOldFile,'(A)',advance='no',iostat=io,end=500)tch
        if(io.eq.-2) lines = lines + 1
        if(tch.ne.ch)cycle
        do q = 2, len(word)
            read(word(q:q+1), '(A)',end=500)ch
            read(unitOldFile,'(A)',advance='no',iostat=io,end=500)tch
            if(tch.ne.ch)then
                read(word, '(A)',iostat = io)ch
                exit
            endif
            if(q.eq.len(word))then
                searchWordCounting = .true.
                return
            endif
        enddo
    enddo
500 continue
    searchWordCounting = .false.
end function searchWordCounting

! Найти нужное слово, переписывая все строчки, кроме самого слова.
logical function searchWordRewritingButNotTheWord(word)
    character(len=:), allocatable, intent(in)   :: word
    character(len=1)                            :: ch
    character(len=1)                            :: tch
    integer                                     :: io
    integer                                     :: q
    read(word, '(A)',iostat = io)ch
    do
        read(unitOldFile,'(A)',advance='no',iostat=io,end=600)tch
        if(tch.ne.ch .or. io.eq.-2)then
            call writeChrAndNewLine(tch, io)
            cycle
        endif
        do q = 2, len(word)
            read(word(q:q+1), '(A)',end=600)ch
            read(unitOldFile,'(A)',advance='no',iostat=io,end=600)tch
            if(tch.ne.ch .or. io.eq.-2)then
                write(unitNewFile,'(A)',advance='no')word(1:(q-1))
                call writeChrAndNewLine(tch, io)
                read(word, '(A)',iostat = io)ch
                exit
            endif
            if(q.eq.len(word))then
                searchWordRewritingButNotTheWord = .true.
                return
            endif
        enddo
    enddo
600 continue
    searchWordRewritingButNotTheWord = .false.
end function searchWordRewritingButNotTheWord


! Дописать заданное количество пустых строчек
subroutine writeNewLine(num)
    integer, intent(in) :: num
    integer             :: i
    do i = 1, num
        write(unitNewFile,'(A)',advance='yes')''
    enddo
end subroutine writeNewLine

! Переписать начатую строку из файла unitOldFile в файл unitNewFile.
subroutine rewriteStartedLine(ch1, io1)
    character (len=1), intent(in) :: ch1
    character (len=1)             :: ch
    integer, intent(in)           :: io1
    integer                       :: io
    ch = ch1
    io=io1
    do while(io > -1)
        write(unitNewFile,'(A)',advance='no')ch
        read(unitOldFile,'(A)',advance='no',iostat=io,end=300)ch
    enddo
    if(io.eq.-2) write(unitNewFile,'(A)')''
    return
300 write(*,*)'ERROR: END OF FILE. The file has wrong format'
end subroutine rewriteStartedLine

! Переписать не начатую строчку из файла.
subroutine rewriteLine
    character (len=1)             :: ch
    integer                       :: io
    io = 0
    do while(io > -1)
        read(unitOldFile,'(A)',advance='no',iostat=io,end=200)ch
        write(unitNewFile,'(A)',advance='no')ch
    enddo
    if(io.eq.-2) write(unitNewFile,'(A)')''
    return
200 write(*,*)'ERROR: END OF FILE. The file has wrong format'
end subroutine rewriteLine

! Записать в получаемый файл новую строку, если аргумент равен -2
subroutine rewriteNewLine(io)
integer, intent(in) :: io
    if(io.eq.-2) write(unitNewFile,'(A)')''
end subroutine rewriteNewLine

! Записать в получаемый файл string без перевода строки
subroutine anWrite(string)
    character(len=:), allocatable, intent(in) :: string
    write(unitNewFile,'(A)',advance='no') string
end subroutine anWrite

! Записать в получаемый файл string с переводом строки
subroutine ayWrite(string)
    character(len=:), allocatable, intent(in) :: string
    write(unitNewFile,'(A)',advance='yes')string
end subroutine ayWrite

! Записать символ и новую строку, если надо
subroutine writeChrAndNewLine(ch, io)
    character(len=1), intent(in) :: ch
    integer, intent(in)                       :: io
    if(io > -1) then
        write(unitNewFile,'(A)',advance='no')ch
    else
        write(unitNewFile,'(A)',advance='yes')ch
    endif
end subroutine writeChrAndNewLine

end module SearchWordModule
