module ColumnTableModule
implicit none

    Type ColumnTable
        real,    private, allocatable, dimension(:)             :: keep
        integer, private                                        :: lines
        integer, private                                        :: actualLine = 1
    contains
        procedure           :: setLine
        procedure           :: isFull
        procedure           :: isEmpty
        procedure           :: getNext
        procedure           :: resetCount
        procedure           :: getLimitLine
        procedure           :: decrementCount
  !      final               :: delete_Column
    end type ColumnTable

    interface ColumnTable
        procedure :: create_columnTable
    end interface
contains

! Уменьшает счётчик чисел на еденицу.
    subroutine decrementCount(this)
        class(ColumnTable)  :: this
        this%actualLine = this%actualLine - 1
    end subroutine decrementCount

! Обнуляет счётчик чисел.
    subroutine resetCount(this)
        class(ColumnTable)    :: this
        !write(*,*) this%actualLine

        this%actualLine = 1
    end subroutine resetCount

! Узнать максимально допустимое количество линий в колонне. Определяется при создании колонны.
    integer function getLimitLine(this)
        class(ColumnTable),intent(in)    :: this
        getLimitLine = this%lines
    end function getLimitLine

! Конструктор.
    function create_columnTable(line) result(this)
        type (ColumnTable)           :: this
        integer, intent(in)     :: line
        allocate(this%keep(line))
        this%lines = line
    end function create_columnTable

 !   subroutine delete_Column(this)
 !       type(ColumnTable) :: this
 !       if(allocated(this%keep)) deallocate(this%keep)
    !end subroutine delete_Column

! Прочитать следующее число из колонны. Одновременно увеличивает счётчик чисел на еденицу.
    real function getNext(this)
        class(ColumnTable)    :: this
        !if(actualLine > lines)then
        !    write(*,*)'Error, try to get lines more than possible'
        !endif
        getNext = this%keep(this%actualLine)
        this%actualLine = this%actualLine + 1
    end function getNext

! Добавить число в колонну.
    subroutine setLine(this, number)
        real, intent(in) ::number
        class(ColumnTable)        :: this
        !if(this%actualLine .eq. 2) write(*,'(2X,F8.4)',advance='no')number

        if(this%actualLine > this%lines)then
            write(*,*)'ERROR. Too much lines, max :', this%lines
            return
        endif
        this%keep(this%actualLine) = number
        this%actualLine = this%actualLine + 1
    end subroutine setLine

! Заполнена ли колонна до полна?
    logical function isFull(this)
        class(ColumnTable),intent(in)    :: this
        if(this%actualLine > this%lines)then
            isFull = .true.
        else
            isFull = .false.
        endif
    end function isFull

! Пустая ли колонна?
    logical function isEmpty(this)
        class(ColumnTable),intent(in)    :: this
        if(this%actualLine.eq.1)then
            isEmpty = .true.
        else
            isEmpty = .false.
        endif
    end function isEmpty

end module ColumnTableModule
