program PROGRAM
use FileListModule
use ConstantModule
use SearchSectionModule
use countLinesInTableModule
use WriterModule
implicit none
character (len=:), allocatable  :: str              !Строка.
character (len=1)               :: ch               !Рабочая буква.
character (len=:), allocatable  :: line             !
integer                         :: io               !Для ошибок ввода-вывода.
integer                         :: i, j             !Для разного использования.
integer                         :: nfl              ! no. of field levels
type(Writer),target             :: wrt
type(Writer),pointer            :: wr

wr=>wrt

allocate(line, source='NO. OF FILLED LEVELS    =      ')

! Просим систему создать файл - список файлов.
if(.NOT.makeFileList()) goto 9999
open(unit=1, file=nameOfTMPFile, encoding='UTF-8')
i = 0
j = 0
! Ищем в списке файлов строку с наибольшей длинной.
do
    read(1,'(A)',end=5,advance='no',iostat=io)ch
    if(io == -2)then
        if(j < i) j = i
        i = 0
        cycle
    endif
    i = i + 1
enddo
5   continue
allocate(character(len=j) :: str)
rewind 1
! Читаем каждый из этих файлов.
do
    read(1,'(A)', end=10, iostat=io) str
    write(*,*)trim(str)
    open(unit=unitOldFile,file=trim(str),encoding='UTF-8')
    if(.not.searchWord(line))goto 9999
    read(unitOldFile,'(I5)',end=9999) nfl
    if(.not.countLinesInTable(wr)) goto 9999
    close(unitOldFile)
    open(unit=unitOldFile,file=trim(str),encoding='UTF-8')
    open(unit=unitNewFile,file=trim(str)//'.dat',encoding='UTF-8')
    call sectionAbstract(wr,nfl)
    close (unitOldFile)
    close (unitNewFile)
enddo
10    continue
close (1, status='delete')
9999  continue
deallocate(str)
deallocate(line)

contains

subroutine readFile(fileName)
    character(len=:),allocatable    :: fileName
    character(len=200)              :: line
    integer                         :: io2
    write(*,'(A)',advance='no')fileName//'......'
    open(unit=unitOldFile,file=trim(fileName),encoding='UTF-8')
    open(unit=unitNewFile,file=trim(fileName)//'.dat',encoding='UTF-8')
    do
        read(unitOldFile,'(A)',end=20,advance='no',iostat=io2) line
        !if(index(line,'General Reference for PM7:') > 0)

        if(io2 .eq. -2) then
            write(unitNewFile,'(A)',advance='yes')''
            cycle
        endif
        if(line .eq. '*')then
            do
                write(unitNewFile,'(A)',advance='no')line
                read(unitOldFile,'(A)',end=20,advance='no',iostat=io2)line
                if(io2 .eq. -2)then
                    write(unitNewFile,'(A)',advance='yes')''
                    exit
                endif
            enddo
        endif
    enddo
20  continue
    write(*,*)'ok'
    close (unitOldFile)
    close (unitNewFile)
end subroutine readFile

!subroutine create
end program PROGRAM
