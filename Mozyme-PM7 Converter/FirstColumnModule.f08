module FirstColumnModule
    implicit none
    type FirstColumn
        character(len=12), allocatable, dimension(:)   :: keep
        integer                                                 :: lines
        integer                                                 :: actualLine = 1
    contains
        procedure :: getNext
        procedure :: setLine
        procedure :: resetCount
        procedure :: getLimitLine
        procedure :: decrementCount
!        procedure :: destructorFirstColumn
    end type FirstColumn

    interface FirstColumn
        procedure :: create_firstColumn
    end interface

contains

! Конструктор.
    function create_firstColumn(line) result(this)
        type (FirstColumn)      :: this
        integer, intent(in)     :: line
        allocate(this%keep(line))
        this%lines = line
    end function create_firstColumn

! Обнуляет счётчик чисел.
    subroutine resetCount(this)
        class(FirstColumn)    :: this
        this%actualLine = 1
    end subroutine resetCount

! Узнать максимально допустимое количество линий в колонне. Определяется при создании колонны.
    integer function getLimitLine(this)
        class(FirstColumn),intent(in)    :: this
        getLimitLine = this%lines
    end function getLimitLine

    ! Добавить строку в колонну.
    subroutine setLine(this, str)
        class(FirstColumn)            :: this
        character(len=12), intent(in) :: str
        if(this%actualLine > this%lines)then
            write(*,*)'ERROR fc. Too much lines, max :', this%lines
            return
        endif
        this%keep(this%actualLine) = str
        this%actualLine = this%actualLine + 1
    end subroutine setLine

    ! Прочитать следующую строку из колонны. Одновременно увеличивает счётчик чисел на еденицу.
    character(len=12) function getNext(this)
        class(FirstColumn)  :: this
        !if(actualLine > lines)then
        !    write(*,*)'Error, try to get lines more than possible'
        !endif
        getNext = this%keep(this%actualLine)
        this%actualLine = this%actualLine + 1
    end function getNext

! Уменьшает счётчик чисел на еденицу.
    subroutine decrementCount(this)
        class(FirstColumn)  :: this
        this%actualLine = this%actualLine - 1
    end subroutine decrementCount

end module FirstColumnModule
