module WriterModule
use ColumnTableModule, only: ColumnTable
use FirstColumnModule, only: FirstColumn
use ConstantModule
implicit none
    Type Writer
        type(ColumnTable), dimension(15),private    :: columns
        type(FirstColumn),private                   :: fColumn
        integer,private                             :: actualColumn = 0
        integer, private, dimension(15)             :: orderColumns
    contains
         procedure :: writeAll
         procedure :: addNumber
         procedure :: write8
         procedure :: add2FirstColumn
!        procedure :: destructor
    end type Writer

    interface Writer
        procedure   :: create_Writer
    end interface
contains

! Конструктор.
    function create_Writer(lines) result(this)
        type(Writer)        :: this
        integer, intent(in) :: lines
        integer             :: i
        do i = 1, 15
            this%columns(i) = columnTable(lines)
            this%orderColumns(i) = i
        enddo
        this%fColumn = firstColumn(lines)
    end function create_Writer

! Записываем строки в заглавную колонну. Длинна строки 12 символов.
    subroutine add2FirstColumn(this, str)
        class(Writer)       :: this
        character(len=12)   :: str
        call this%fColumn%setLine(str)
    end subroutine add2FirstColumn

! добавить число в колонну.
    subroutine addNumber(this, column, number)
        integer, intent(in) :: column
        real, intent(in) :: number
        class(Writer)       :: this
!        write(*,'(F8.4,A,I2,1X,I2,1X,I1,2X)',advance='no') number, '-',this%orderColumns(this%actualColumn+column) &
!            ,this%actualColumn,column
   !     write(*,'(I4,I4,F10.4)') column, this%actualColumn+column, number
        call this%columns(this%orderColumns(this%actualColumn+column))%setLine(number)
        if(this%Columns(this%orderColumns(this%actualColumn+column))%isFull())then
            if(this%actualColumn+column.eq.15)then
                call this%write8
                return
            endif
            if(this%columns(this%orderColumns(this%actualColumn+column+1))%isEmpty())then
                this%actualColumn = this%actualColumn + column
                if(this%actualColumn > 7)then
                    call this%write8
                endif
            endif
        endif
    end subroutine addNumber

! Записать 8 следующих колонн. Одновременно обнуляет их.
    subroutine write8 (this)
        class(Writer)           :: this
        integer                 :: i, j
        integer, dimension(7)   :: ii
 !       write(*,*)'write'
        do i = 1, 8
            call this%columns(this%orderColumns(i))%resetCount
        enddo
        call this%fColumn%resetCount
! Записываем '   Root No.  '
        write(unitNewFile,'(A)', advance='no')this%fColumn%getNext()
        write(unitNewFile,'(A)', advance='no')' '
! Записываем номера колонн.
        do i = 1, 8
            write(unitNewFile, '(I6, 4X)', advance='no')int(this%columns(this%orderColumns(i))%getNext())
            call this%columns(this%orderColumns(i))%decrementCount
        enddo
! Записываем расширенные номера колонн с буквой А.
        write(unitNewFile, '(A)')
        write(unitNewFile, '(A)')
        write(unitNewFile, '(13X)', advance='no')
        do i = 1, 8
            write(unitNewFile, '(I5,1X,A,3X)', advance='no')int(this%columns(this%orderColumns(i))%getNext()),'A'
        enddo
        write(unitNewFile, '(A)')
        write(unitNewFile, '(A)')
! Записываем заголовок.
        write(unitNewFile, '(A)', advance='no')this%fColumn%getNext()
        do i = 1, 8
            write(unitNewFile, '(F10.3)', advance='no')this%columns(this%orderColumns(i))%getNext()
        enddo
        write(unitNewFile, '(A)')
        write(unitNewFile, '(A)')
! Записываем содержание главной таблицы.
        do j = 3, this%columns(1)%getLimitLine()
            write(unitNewFile,'(A)', advance='no')this%fColumn%getNext()
            do i = 1, 8
                write(unitNewFile,'(F10.4)', advance='no')this%columns(this%orderColumns(i))%getNext()
            enddo
            write(unitNewFile,'(A)')
        enddo
        write(unitNewFile, '(A)')
        write(unitNewFile, '(A)')
! Содержание главной таблицы закончилось. Очищаем колонны.
        do i = 1, 8
            call this%columns(this%orderColumns(i))%resetCount
        enddo
        do i = 1, 7
            ii(i) = this%orderColumns(i)
            this%orderColumns(i) = this%orderColumns(i+8)
        enddo
        j = this%orderColumns(8)
        do i = 8, 14
            this%orderColumns(i) = ii(i-7)
        enddo
        this%orderColumns(15) = j
        this%actualColumn = this%actualColumn - 8
    end subroutine write8

! Записать оставшиеся колонны, если они полные.
    subroutine writeAll(this)
    class(writer)           :: this
    logical, dimension(15)  :: mask
    integer                 :: i, j
    logical                 :: b
        call this%fColumn%resetCount
        do i = 1, 15
            if(this%columns(this%orderColumns(i))%isFull())then
                mask(i) = .true.
                b = .true.
                call this%columns(this%orderColumns(i))%resetCount
            else
                mask(i) = .false.
            endif
        enddo
        if(.not.b) return
! Записываем '   Root No.  '
        write(unitNewFile,'(A)', advance='no')this%fColumn%getNext()
        write(unitNewFile,'(A)', advance='no')' '
! Записываем номера колонн.)
        do i = 1, 15
            if(mask(i)) then
                write(unitNewFile, '(I6, 4X)', advance='no')int(this%columns(this%orderColumns(i))%getNext())
                call this%columns(this%orderColumns(i))%decrementCount
            endif
        enddo
! Записываем расширенные номера колонн с буквой А.
        write(unitNewFile, '(A)')
        write(unitNewFile, '(A)')
        write(unitNewFile, '(13X)', advance='no')
        do i = 1, 15
            if(mask(i)) then
                write(unitNewFile, '(I5,1X,A,3X)', advance='no')int(this%columns(this%orderColumns(i))%getNext()),'A'
            endif
        enddo
        write(unitNewFile, '(A)')
        write(unitNewFile, '(A)')
! Записываем заголовок.
        write(unitNewFile, '(A)', advance='no')this%fColumn%getNext()
        do i = 1, 15
                if(mask(i)) then
                    write(unitNewFile, '(F10.3)', advance='no')this%columns(this%orderColumns(i))%getNext()
                endif
        enddo
        write(unitNewFile, '(A)')
        write(unitNewFile, '(A)')
        do j = 3, this%columns(1)%getLimitLine()
            write(unitNewFile,'(A)', advance='no')this%fColumn%getNext()
            do i = 1, 15
                if(mask(i)) then
                    write(unitNewFile,'(F10.4)', advance='no')this%columns(this%orderColumns(i))%getNext()
                endif
            enddo
            write(unitNewFile,'(A)')
        enddo
        write(unitNewFile, '(A)')
        write(unitNewFile, '(A)')
! Записываем содержание главной таблицы закончилось. Очищаем колонны.
        do i = 1, 15
            call this%columns(this%orderColumns(i))%resetCount
        enddo
        this%actualColumn = 0
    end subroutine

end module WriterModule
