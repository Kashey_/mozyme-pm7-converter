module SearchSectionModule
use ConstantModule
use SearchWordModule
use WriterModule
implicit none

contains

subroutine sectionAbstract(wr, nfl)
    character(len=:),allocatable    :: line
    character(len=1)                :: ch
    character(len=50)               :: errorString
    integer                         :: io, i, q, inum
    integer, intent(in)             :: nfl
!    integer, dimension(1:8)         :: columns
!    real                            :: title
    real                            :: rnum
    type(Writer),pointer            :: wr
! Перво-наперво ищем строку " General Reference for PM7:"
! Всё, что до этой строки просто переписываем без изменений.
    do
        do
!Первый символ должен быть пробел.
            errorString = 'Can''t find first space'
            read(unitOldFile,'(A)',end=100,iostat=io,advance='no') ch
            if(ch.ne.' ' .or. io.eq.-2)then
                call rewriteStartedLine(ch,io)
                cycle
            else
                write(unitNewFile,'(A)',advance='no')' '
            endif
!Второй символ "G".
            errorString = 'Can''t find '' G'''
            read(unitOldFile,'(A)',end=100,iostat=io,advance='no') ch
            if(ch.ne.'G') then
                call rewriteStartedLine(ch,io)
                cycle
            else
                write(unitNewFile,'(A)',advance='no')ch
            endif
            errorString = 'Can'' find eneral Reference for PM7:'
!Ищем оставшиеся символуы "eneral Reference for PM7:".
            allocate(character(len=25) :: line)
            read(unitOldFile,'(A)',end=100,iostat=io,advance='no') line
            call anWrite(line)
            read(unitOldFile,'(A)',end=100,iostat=io,advance='no') ch
            call rewriteStartedLine(ch,io)
            if(line.ne.'eneral Reference for PM7:') cycle
            deallocate(line)
!Нашли " General Reference for PM7:".
!Дальше несколько малозначащих строк.
            do io = 1, 5
                call rewriteLine
            enddo
            call writeNewLine(3)
            write(unitNewFile,'(A)',advance='yes')'      MOLECULAR POINT GROUP   :   C1  '
            write(unitNewFile,'(A)',advance='yes')
            write(unitNewFile,'(A)',advance='yes')
            write(unitNewFile,'(A)',advance='no')'      RHF CALCULATION, NO. OF DOUBLY OCCUPIED LEVELS ='
            write(unitNewFile,'(I5)',advance='yes') nfl
            write(unitNewFile,'(A)',advance='yes')
            write(unitNewFile,'(A)',advance='yes')
! Надеюсь, что она не важна.
            write(unitNewFile,'(A)',advance='yes')' -------------------------------------------------------------------------------'
! Ищем "mozyme" в начале строки.
            errorString = 'Can''t find ''mozyme'''
            allocate(character(len=6) :: line)
            do
                read(unitOldFile,'(A)',advance='no',iostat=io,end=100)line
                if(io.eq.-2) cycle
                if(line.eq.'mozyme') exit
                read(unitOldFile,'(A)',advance='yes',iostat=io,end=100)ch
            enddo
            deallocate(line)
! Нашли "mozyme". Пока не переделываю, переписываю так, как есть, т.е. mozyme.
! Если не будет читать, предёться поменять на PM7.
            write(unitNewFile,'(A)',advance='no')'mozyme'
            allocate(character(len=7) :: line)
            errorString = 'Can''t find vectors'
            line = 'vectors'
            if(.not.searchWordRewriting(line)) goto 100
            deallocate(line)
            read(unitOldFile,'(A)')
! Переписали "eigen allvec vectors".
            write(unitNewFile,'(A)',advance='yes')''
! Теперь переписываем всё до слов "           Empirical Formula: "
            errorString = 'Can''t find ''           Empirical Formula: '''
            allocate(character(len=30) :: line)
            line = 'Empirical Formula: '
            if(.not.searchWordRewriting(line)) goto 100
            deallocate(line)
            call rewriteLine
            call writeNewLine(3)
            write(unitNewFile,'(A)')'      MOLECULAR POINT GROUP   :   C1  '
            call writeNewLine(2)
            write(unitNewFile,'(A)')'                EIGENVECTORS  '
            call writeNewLine(2)
            read(unitOldFile, *)ch
! Теперь переписываем координаты.
            errorString = 'Can''t find ''Root No.'''
            allocate(character(len=8) :: line)
            line = 'Root No.'
! Находим Root No.
            if(.not.searchWord(line)) goto 100
            deallocate(line)
            errorString = 'End of Root'
            allocate(character(len=12) :: line)
            do
! Переписываем номера колонн.
                i = 8
                do q = 1, i
                    read(unitOldFile, '(I8)', advance='no',iostat=io)inum
                    rnum=real(inum)
                    if(io.eq.-2)then
                        i = q - 1
                        exit
                    endif
                    call wr%addNumber(q, rnum)
                enddo
                if(i.eq.8) read(unitOldFile, '(A)')
                read(unitOldFile, '(A)')
! Переписываем заголовок
                read(unitOldFile,'(TR12)',advance='no')
                do q = 1, i
                    read(unitOldFile,'(F8.3)',advance='no') rnum
                    call wr%addNumber(q, rnum)
                enddo
                read(unitOldFile, '(A)')
                read(unitOldFile, '(A)')
! Переписываем содержание главной таблицы
                do
                    read(unitOldFile, '(A12)', advance='no', iostat=io,end=100)line
                    if(io.eq.-2) exit
                    do q = 1, i
                        read(unitOldFile, '(F8.4)', advance='no')rnum
                        call wr%addNumber(q, rnum)
                    enddo
                    read(unitOldFile, '(A)')
                enddo
! Содержание главной таблицы закончилось.
! Проверяем, надо ли возвращаться.
                read(unitOldFile, '(A)')line
                if(line.eq.'  Virtual Or')then
                    read(unitOldFile, '(A)')
                    read(unitOldFile, '(A)')
                    read(unitOldFile, '(A)')
                    read(unitOldFile, '(12A)',advance='no')line
                    cycle
                endif
                read(unitOldFile, '(A12)', advance='no', iostat=io,end=100)line
                if(line.eq.'    Root No.') cycle
                call wr%writeAll


! Дописываем почти до самого конца.
                write(unitNewFile, '(A)', advance='no')line
                errorString = 'Not found ''DATA FOR GRAPH WRITTEN TO DISK'''
                deallocate(line)
                allocate(character(len=35) :: line)
                line = '     DATA FOR GRAPH WRITTEN TO DISK'
                if(.not.searchWordRewritingButNotTheWord(line))goto 100
                deallocate(line)
                errorString = ' Ok'
                read(unitOldFile, '(A)')
                read(unitOldFile, '(A)')
                io = 0
                do
                    read(unitOldFile,'(A)',advance='no',iostat=io,end=100)ch
                    call writeChrAndNewLine(ch,io)
                enddo
            enddo
        exit
        enddo
        exit
    enddo
    return
100 write(*,*)'- ', errorString
end subroutine sectionAbstract

end module SearchSectionModule
