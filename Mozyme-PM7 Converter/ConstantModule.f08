module ConstantModule
implicit none

integer, parameter              :: unitOldFile = 2  !Поток ввода - вывода модифицируемого файла.
integer, parameter              :: unitNewFile = 3  !Поток ввода - вывода конечного файла.

end module ConstantModule
