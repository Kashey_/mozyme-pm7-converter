Module FileListModule
implicit none

character (len=5)                       :: nameOfTMPFile = 'tmp12'  !Временый файл со списком файлов.
character (len=1),              private :: ch                       !Рабочая буква.
character (len=:), allocatable, private :: extensionOpenFile        !Расширение модифицируемых файлов.
logical,                        private :: isExist                  !Есть ли файл или его нет?
integer,                        private :: i                        !Временная переменная.

contains

! Просит систему создать файл и переписать в него все файлы, находящиеся в этой папке
! с заданым пользователем расширением. Имя файла в nameOfTMPFile. Если такой файл уже
! существует, может  переписать его, если пользователь выразит согласие.
! Возвращаесое значение зависит от создания файла.
! Расширение файла не должно быть более 10-ти символов.

logical function makeFileList()
    inquire(file=nameOfTMPFile,exist=isExist)
    if(isExist) then
        write(*,*)'File '''//nameOfTMPFile//''' will be deleted'
        do
            write(*,'(A)',advance='no')'Ok? Y/N '
            read(*,*) ch
            if (ch .eq. 'Y' .or. ch .eq. 'y') then
                exit
            else if (ch .eq. 'N') then
                makeFileList = .FALSE.
                return
            endif
        enddo
    endif
    allocate(character(len=11) :: extensionOpenFile)
    do
        write(*,'(A)',advance='no') 'Enter extension of modifiable files: *.'
        read (*,*) extensionOpenFile
        if(len(trim(extensionOpenFile)) > 10) then
            write(*,'(A)',advance='no')'Is it nessesery to use long extension, please write Y '
            read(*,*)ch
            if(ch == 'Y'.or.ch == 'y')then
                write(*,'(A)',advance='no')'Write length of extention: '
                read(*,*)i
                if(i .eq. -1) print*, i
                if(i > 1 .and. i < 1000) then
                    deallocate (extensionOpenFile)
                    allocate (character(len=i) :: extensionOpenFile)
                    write(*,'(A)',advance='no') 'Enter extension of modifiable files: *.'
                    read(*,*) extensionOpenFile
                    exit
                else
                    write(*,*)'Too long extension. Less then 1000, please'
                endif
            endif
        else
            exit
        endif
    enddo
    call execute_command_line('dir /b/a-d/on *.'//trim(extensionOpenFile)//' > '//nameOfTMPFile, .TRUE.)
    deallocate(extensionOpenFile)
    makeFileList = .TRUE.
end function

end Module
