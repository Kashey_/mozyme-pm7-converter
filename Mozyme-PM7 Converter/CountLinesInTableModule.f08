module countLinesInTableModule
use SearchWordModule
use WriterModule
implicit none
contains
! Cчитаем количество линий между "Root No.", отнимаем пустые строчки и добавляем строчки на заголовок и подзаголовок.
! Файл должен быть открыт для прямого доступа.
logical function countLinesInTable(wr)
    character(len=:),allocatable    :: line
    character(len=1)                :: ch
    type(Writer),intent(out),pointer:: wr
    integer                         :: io
    integer                         :: numLines
    allocate(character(len=8) :: line)
    line = 'Root No.'
    if(.not.searchWord(line)) then
        write(*,*)'Can''t count number of lines, ''Root No.'' not find'
        countLinesInTable = .false.
        return
    endif
    numLines = 2
    read(unitOldFile,'(A)',end=9998)
    read(unitOldFile,'(A)',end=9998)
    read(unitOldFile,'(A)',end=9998)
    read(unitOldFile,'(A)',end=9998)
    do
        read(unitOldFile,'(A)',advance='no',iostat=io,end=9998)ch
        if(io.eq.-2) exit
        read(unitOldFile,'(A)',end=9998)
        numLines = numLines + 1
    enddo
    wr = create_Writer(numLines)
    read(unitOldFile,'(A)',end=9998)
    read(unitOldFile,'(A)',end=9998)
    read(unitOldFile,'(A)',end=9998)
    read(unitOldFile,'(A)',end=9998)
    read(unitOldFile,'(A)',end=9998)
    call wr%add2FirstColumn('   Root No. ')
    call wr%add2FirstColumn('            ')
    deallocate(line)
    allocate(character(len=12) :: line)
    do
        read(unitOldFile, '(A12)',advance='no',iostat=io,end=9998)line
        if(io.eq.-2) exit
        call wr%add2FirstColumn(' '//line(1:6)//line(8:12))
        read(unitOldFile,'(A)',end=9998)
    enddo
    countLinesInTable = .true.
    return
9998 write(*,*)'Can''t count number of lines, unexepted End Of File'
    countLinesInTable = .false.
    return

end function countLinesInTable

end module countLinesInTableModule
